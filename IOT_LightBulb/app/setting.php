<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class setting extends Model
{
    //
    protected $table = 'light_setting';
    
    public function light()
    {
        return $this->belongsTo('App\light', 'name', 'name');
    }
}
