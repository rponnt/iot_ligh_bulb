<?php

namespace App\Http\Controllers;
require("../vendor/autoload.php");
use App\light;
use App\setting;
use App\log;
use App\subScriber;
use \Illuminate\Http\Request;
use \Illuminate\Http\Response;
use App\Services\PayUService\Exception;

class LightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     private $host;
     private $port;
     private $clientID;
     private $mqtt;
     private $username;
     private $password;


     //set publicer for send somthing
     function __construct() {

        try{
            $this->host = '	m14.cloudmqtt.com';
            $this->port = 	12540;
            $this->clientID = 'instance1_001638867';


            $this->mqtt = new \Lightning\App( $this->host, $this->port,  $this->clientID);
            $this->mqtt->connect_auto();
            $mqtt->listen();

            $lightCollections =  light::get();
            // Create a array
            $stack = array();
            //init thread
            foreach ($lightCollections as $light) {
                $stack[] = new subScriber($light->name);
            }
            //start task
            foreach ( $stack as $task ) {
                $task->start();
            }
        }
        catch (\Exception $e) {
            return redirect('/')->withErrors(['Error!',"when init ".$e->getMessage()]);
        }

     }

     

    public function index()
    {
        //
        $lightCollections =  light::get();
        $log = log::get();
        return view('home',['lightCollections' => $lightCollections,'log'=>$log]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $light = $this->validate(request(), [
            'name' => 'required',
            'description' => 'required'
          ]);
          light::create($light);
          return redirect('/');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\light  $light
     * @return \Illuminate\Http\Response
     */
    public function edit(light $light)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\light  $light
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, light $light)
    {
        //


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\light  $light
     * @return \Illuminate\Http\Response
     */
    public function destroy(light $light)
    {
        //

        
        $light->delete();
        return redirect('/')->with('success','light has been  deleted');
    }

    public function setting(Request $request)
    {
        try{
            dd($request->all());
            $light_instance = light::where('name',$request->light_name)->first();
            //dd($request->light_name);
            $check= $this->validate(request(), [
                'instance' => 'required',
                'dim' => 'required',
                'timeOut' => 'required',
            ]);

            $light_instance->setting->light_status= ($request->light != "off")? true : false;//$request->light;
            //dd(($request->light != "false")? true : false);
            $light_instance->setting->motion_status= ($request->motion != "off")? true : false;//$request->motion;
            $light_instance->setting->dim= $request->dim;
            $light_instance->setting->motion_time_out= $request->timeOut;
            $light_instance->setting->save();

            //serialize data model to json format
            $json = json_encode($light_instance->setting);
            //dd($json);
            $this->mqtt = new \Lightning\App( $this->host, $this->port,  $this->clientID);
            if (!$mqtt->connect()) {
                exit(1);
            }

            $mqtt->publish("IOT_LIGHT_BLUB/web/". $light_instance->name,  $json, 1);
            $mqtt->close();
        
            return back()->with('success', 'Setting change complete');
        }
        catch (\Exception $e) {
            return redirect('/')->withErrors(['Error!',"when setting cause: ".$e->getMessage()]);
        }



    }

    public function add(Request $request)
    {
        try{
                //validation
            $this->validate(request(), [
                    'name' => 'required',
                    'description' => 'required',
            ]);

            $uuid =hexdec(uniqid($request->name));
            //light instance
            $light = new light();
            $light->id=$uuid;
            $light->name=$request->name;
            $light->description=$request->description;
            $light->save();

            //light setting
            $setting = new setting();
            $setting->id=$uuid;
            $setting->name = $light->name;
            $setting->light_status = true;
            $setting->motion_status = true;
            $setting->dim = 50;
            $setting->motion_time_out = 12;
            $setting->save();

            $json = json_encode($setting);
            $this->mqtt = new \Lightning\App( $this->host, $this->port,  $this->clientID);
            if (!$mqtt->connect()) {
                exit(1);
            }
            
            $this->mqtt->publish("/Iot_lightBulb/config",  $json, 1);
            $this->mqtt->close();
            
            return redirect('/')->with('success', 'Setting change complete');
        }
        catch(\Exception $e){
            return redirect('/')->withErrors(['Error !',"when add neww artifact: ".$e->getMessage()]);
        }    
    }

    public function view($name)
    {
        $lights=light::get();
        $light_instance = light::where('name',$name)->first();
        return view('setting',['lights'=> $lights,'light_instance' => $light_instance]);
        
    }

    //remove
    public function remove($name)
    {
        try {
            $setting = setting::where('name',$name)->first()->delete();
            $log = log::where('name',$name)->get()->delete();
            $light = light::where('name',$name)->first()->delete();
            return redirect('/')->with('success','light has been  deleted');
        }
        catch (\Exception $e) {
            return redirect('/')->withErrors(['Error !',$e->getMessage()]);
        }        
    }

    public function home()
    {
        //
        try {
            $lightCollections =  light::get();

            $logs = [];
            foreach ($lightCollections as $light) {
                $log = log::where('name',$light->name)->orderBy('time', 'asc')->get();
                array_push($logs,$log);
            }
            $logTime = log::orderBy('time', 'asc')->get();
            return view('home',['lightCollections' => $lightCollections,'logs'=>$logs,'logTime'=>$logTime]);
        }
        catch (\Exception $e) {
            return redirect('/')->withErrors(['msg',$e->getMessage()]);           
        }
    }


}
