<?php

namespace App\Console;
use App\log;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->call(function () {
            //DB::table('recent_users')->delete();
            //get all instane in system
            $lightCollections =  light::get();
            foreach ($lightCollections as $light) {    
                $old_log = log::where('name', $light->name)->orderBy('time', 'desc')->first();
                $new_log = new log();
                $new_log->id=md5(uniqid());
                $new_log->name=$old_log->name;
                $new_log->light_status_on_artifact=$old_log->light_status_on_artifact;
                $new_log->motion_status=$old_log->light_status_on_artifact;
                $new_log->time=new DateTime('now');
                $new_log->save();
            }
        })->everyMinute();;
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
