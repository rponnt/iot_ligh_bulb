<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class light extends Model
{
    //
    protected $table = 'light_instance';

    //for refrence setting model
    public function setting()
    {
        return $this->hasOne('App\setting', 'name', 'name');
    }

    //for refrence log model
    public function log()
    {
        return $this->hasMany('App\log', 'name', 'name');
    }

}
