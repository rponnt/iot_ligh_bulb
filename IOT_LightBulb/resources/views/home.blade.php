<!doctype html>
<html lang="Electric use">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Electric use</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M"
        crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">

</head>

<body>
    <div class="container">
        <div class="row">
            <h1 class="display-1">Electric used</h1>
        </div>
        <div class="row">
            <h2 class="lead">
                All instance
            </h2>
       {{csrf_field()}}
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
            @endif

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </div>
        <div class="row">
            <div class="form-group">
                <label for="scale">Interval</label>
                <select id="scale" name="type">
                            <option val="Minute">Minute</option>
                            <option val="Hours" >Hours</option>
                            <option val="Day">Day</option>
                            <option val="Week" >Week</option>
                            <option val="Month">Month</option>
                            <option val="Year">Year</option>
                    </select>
            </div>
        </div>
        <div class="row">
            <div class="col-6 col-md-4 col-xs-2">
                <div id="datetimepicker" class="input-append date">
                    <input type="text" name="start" id="start"></input>
                    <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                            </span>
                </div>
            </div>
            <div class="col-6 col-md-4 col-xs-2">
                <div id="datetimepicker2" class="input-append date">
                    <input type="text" name="end" id="end"></input>
                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="ct-chart ct-perfect-fourth"></div>
        </div>
        <div class="row">
            <table class="table table-inverse">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Duration</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($lightCollections as $light)

                    <tr>

                        <th scope="row">{{$light->id}}</th>
                        <td> <a href="{{ url('/item/'.$light->name) }}">{{$light->name}}</a></td>
                        <td>20D</td>

                    </tr>
                    </a>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</body>
<script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js">
</script>
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js">
</script>
<script type="text/javascript" src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
</script>
<script type="text/javascript" src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js">
</script>
<script type="text/javascript">
    $('#datetimepicker').datetimepicker({



        format: 'dd-MM-yyyy hh:mm',
        language: 'en',
        collapse: false,
        maskInput: false,
        pickSeconds: false
    });
</script>
<script type="text/javascript">
    $('#datetimepicker2').datetimepicker({



        format: 'dd-MM-yyyy hh:mm',
        language: 'en',
        collapse: false,
        maskInput: false,
        pickSeconds: false
    });
</script>
<script type="text/javascript">
    function computeInterval(log){
       //init set of value
        var val = [];
        //init data
        var temp =0;
        val.push(temp);
        for (j = 0; j < log.length-1; j++) { 

            //check this log has light(on)?
            if((log[j].light_status_on_artifact == 1) &&(log[j+1].light_status_on_artifact == 1)){
                var start_point_interval = new Date(log[j].time);
                var end_point_interval = new Date(log[j+1].time);
                var diffMs = (end_point_interval - start_point_interval);//millisec
                /*
                var diffDays = Math.floor(diffMs / 86400000); // days
                var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
                var diffMins = Math.round(diffMs/60000); // minutes
                */
                var scale = $("#scale").val();
                switch(scale) {
                        case "Minute":
                            temp = temp + Math.round(diffMs/60000);
                            val.push(temp);
                            break;                    
                        case "Hours":
                            temp = temp + Math.round(diffMs/(60000*60));
                            val.push(temp);
                            break;                    
                        case "Day":
                            temp = temp + Math.round(diffMs/(60000*60*24));
                            val.push(temp);
                            break;
                        case "Week":
                            temp = temp + Math.round(diffMs/(60000*60*24*7));
                            val.push(temp);
                            break;
                        case "Month":
                            temp + Math.round(diffMs/(60000*60*24*30))
                            break;
                        case "Year":
                            temp + Math.round(diffMs/(60000*60*24*365))
                            break;
                        default:
                            temp = temp + Math.round(diffMs/60000);
                            val.push(temp);
                            break;
                    }

            }
            else{
                val.push(temp);
            }

        }
        
        return val;
    }
</script>


<script type="text/javascript">
    function computeIntervalSpecific(model,start,end){
        //init set of value
        var val = [];
        //init data
        var temp =0;
        val.push(temp);
        for (j = 0; j < model.length-1; j++) { 

            if(((new Date(model[j].time) - start )>=0) && ((new Date(model[j].time) - end )<=0)){
                //check this log has light(on)?
                if((model[j].light_status_on_artifact == 1) &&(model[j+1].light_status_on_artifact == 1)){
                    var start_point_interval = new Date(model[j].time);
                    var end_point_interval = new Date(model[j+1].time);
                    var diffMs = (end_point_interval - start_point_interval);//millisec
                    /*
                    var diffDays = Math.floor(diffMs / 86400000); // days
                    var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
                    var diffMins = Math.round(diffMs/60000); // minutes
                    */
                    var scale = $("#scale").val();
                    switch(scale) {
                            case "Minute":
                                temp = temp + Math.round(diffMs/60000);
                                val.push(temp);
                                break;                    
                            case "Hours":
                                temp = temp + Math.round(diffMs/(60000*60));
                                val.push(temp);
                                break;                    
                            case "Day":
                                temp = temp + Math.round(diffMs/(60000*60*24));
                                val.push(temp);
                                break;
                            case "Week":
                                temp = temp + Math.round(diffMs/(60000*60*24*7));
                                val.push(temp);
                                break;
                            case "Month":
                                temp + Math.round(diffMs/(60000*60*24*30))
                                break;
                            case "Year":
                                temp + Math.round(diffMs/(60000*60*24*365))
                                break;
                            default:
                                temp = temp + Math.round(diffMs/60000);
                                val.push(temp);
                                break;
                        }

                }
                else{
                    val.push(temp);
                }
            }

        }
        
        return val;

    }
</script>


 <script type="text/javascript">
        var js_data = '<?php echo json_encode($logs); ?>';
        var js_obj_data = JSON.parse(js_data );
        console.log(js_obj_data);



        var instance= [];
        var series =[];
        var label = [];
        // push unique instance name to 
        for(i = 0; i < js_obj_data.length; i++) { 
            console.log(i);
            console.log(js_obj_data[i]);
            instance[i] = js_obj_data[i][0].name;
            console.log(instance);
            //check interval time
            series[i]= computeInterval(js_obj_data[i]);
            for(index = 0; index < js_obj_data[0].length; index++) { 
                label[index] = js_obj_data[0][index].time;
            }
        }
        //set default datepicker
        $("#start").val(new Date(label[0]));
        $("#end").val(new Date(label[label.length-1]));
        // Our labels and three data series
        console.log(label);    
        console.log(series);
        var data = {
        labels: label,
        series: series
        };

        // We are setting a few options for our chart and override the defaults
        var options = {
        // Don't draw the line chart points
        showPoint: true,
        // Disable line smoothing
        lineSmooth: true,
        // X-Axis specific configuration
        axisX: {
            // We can disable the grid for this axis
            showGrid: true,
            // and also don't show the label
            showLabel: true
        },
        // Y-Axis specific configuration
        axisY: {
            // Lets offset the chart a bit from the labels
            offset: 100,
            // The label interpolation function enables you to modify the values
            // used for the labels on each axis. Here we are converting the
            // values into million pound.
            labelInterpolationFnc: function(value) {
            return  value ;
            }
        }
        };

        // All you need to do is pass your configuration as third parameter to the chart function
        new Chartist.Line('.ct-chart', data, options);


        // on scale change 
        
$( "#scale" ).change(function() {
   var js_data = '<?php echo json_encode($logs); ?>';
        var js_obj_data = JSON.parse(js_data );
        console.log(js_obj_data);

        var instance= [];
        var series =[];
        // push unique instance name to 
        for(i = 0; i < js_obj_data.length; i++) { 
            console.log(i);
            console.log(js_obj_data[i]);
            instance[i] = js_obj_data[i][1].name;
            console.log(instance);
            //check interval time
            series[i]= computeInterval(js_obj_data[i]);
            
        }
            // Our labels and three data series
        console.log(series);
        var data = {
        labels: label,
        series: series
        };

        // We are setting a few options for our chart and override the defaults
        var options = {
        // Don't draw the line chart points
        showPoint: true,
        // Disable line smoothing
        lineSmooth: true,
        // X-Axis specific configuration
        axisX: {
            // We can disable the grid for this axis
            showGrid: true,
            // and also don't show the label
            showLabel: true
        },
        // Y-Axis specific configuration
        axisY: {
            // Lets offset the chart a bit from the labels
            offset: 100,
            // The label interpolation function enables you to modify the values
            // used for the labels on each axis. Here we are converting the
            // values into million pound.
            labelInterpolationFnc: function(value) {
            return  value ;
            }
        }
        };

        // All you need to do is pass your configuration as third parameter to the chart function
        new Chartist.Line('.ct-chart', data, options);
});


  //when date picker change
     $('#datetimepicker').datetimepicker().on('changeDate',function(e){

         
        var js_data = '<?php echo json_encode($logs); ?>';
        var js_obj_data = JSON.parse(js_data );
        console.log(js_obj_data);



        var label = [];
        // push unique instance name to 
        for(i = 0; i < js_obj_data.length; i++) { 
            //check interval time
            for(index = 0; index < js_obj_data[0].length; index++) { 
                label[index] = js_obj_data[0][index].time;
            }
        }

        var start =  new Date(e.localDate);
        var end =  new Date($( "#end" ).val());

        if(((end-start)>=0) && (start-(new Date(label[0]))>=0) &&(start-(new Date(label[label.length-1]))<0) &&(end-(new Date(label[0]))>=0) &&(end-(new Date(label[label.length-1]))<=0)){
            
            var js_data = '<?php echo json_encode($logs); ?>';
            var js_obj_data = JSON.parse(js_data );
            console.log(js_obj_data);



            var instance= [];
            var series =[];
            var label_specific = [];
            // push unique instance name to 
            for(i = 0; i < js_obj_data.length; i++) { 
                console.log(i);
                console.log(js_obj_data[i]);
                instance[i] = js_obj_data[i][0].name;
                console.log(instance);
                //check interval time
                series[i]= computeIntervalSpecific(js_obj_data[i],start,end);
                label_specific = [];
                for(index = 0; index < js_obj_data[0].length; index++) { 

                    if(((new Date(js_obj_data[0][index].time) - start )>=0) && ((new Date(js_obj_data[0][index].time) - end )<=0)){
                        label_specific.push(js_obj_data[0][index].time);
                    }
                }
            }
            console.log(series);
            console.log(label_specific);
            var data = {
            labels: label_specific,
            series: series
            };

            // We are setting a few options for our chart and override the defaults
            var options = {
            // Don't draw the line chart points
            showPoint: true,
            // Disable line smoothing
            lineSmooth: true,
            // X-Axis specific configuration
            axisX: {
                // We can disable the grid for this axis
                showGrid: true,
                // and also don't show the label
                showLabel: true
            },
            // Y-Axis specific configuration
            axisY: {
                // Lets offset the chart a bit from the labels
                offset: 100,
                // The label interpolation function enables you to modify the values
                // used for the labels on each axis. Here we are converting the
                // values into million pound.
                labelInterpolationFnc: function(value) {
                return  value ;
                }
            }
            };

            // All you need to do is pass your configuration as third parameter to the chart function
            new Chartist.Line('.ct-chart', data, options);


        }
        else{
            alert('You choose date out of bound');
            $("#start").val(new Date(label[0]));
            $("#end").val(new Date(label[label.length-1]));
        }

    });

     $('#datetimepicker2').datetimepicker().on('changeDate',function(e){


         
         
        var js_data = '<?php echo json_encode($logs); ?>';
        var js_obj_data = JSON.parse(js_data );
        console.log(js_obj_data);



        var label = [];
        // push unique instance name to 
        for(i = 0; i < js_obj_data.length; i++) { 
            //check interval time
            for(index = 0; index < js_obj_data[0].length; index++) { 
                label[index] = js_obj_data[0][index].time;
            }
        }

        var start =  new Date($( "#start" ).val());
        var end =  new Date(e.localDate);

        if(((end-start)>=0) && (start-(new Date(label[0]))>=0) &&(start-(new Date(label[label.length-1]))<0) &&(end-(new Date(label[0]))>=0) &&(end-(new Date(label[label.length-1]))<=0)){
            
            var js_data = '<?php echo json_encode($logs); ?>';
            var js_obj_data = JSON.parse(js_data );
            console.log(js_obj_data);



            var instance= [];
            var series =[];
            var label_specific = [];
            // push unique instance name to 
            for(i = 0; i < js_obj_data.length; i++) { 
                console.log(i);
                console.log(js_obj_data[i]);
                instance[i] = js_obj_data[i][0].name;
                console.log(instance);
                //check interval time
                series[i]= computeIntervalSpecific(js_obj_data[i],start,end);
                label_specific = [];
                for(index = 0; index < js_obj_data[0].length; index++) { 

                    if(((new Date(js_obj_data[0][index].time) - start )>=0) && ((new Date(js_obj_data[0][index].time) - end )<=0)){
                        label_specific.push(js_obj_data[0][index].time);
                    }
                }
            }
            console.log(series);
            console.log(label_specific);
            var data = {
            labels: label_specific,
            series: series
            };

            // We are setting a few options for our chart and override the defaults
            var options = {
            // Don't draw the line chart points
            showPoint: true,
            // Disable line smoothing
            lineSmooth: true,
            // X-Axis specific configuration
            axisX: {
                // We can disable the grid for this axis
                showGrid: true,
                // and also don't show the label
                showLabel: true
            },
            // Y-Axis specific configuration
            axisY: {
                // Lets offset the chart a bit from the labels
                offset: 100,
                // The label interpolation function enables you to modify the values
                // used for the labels on each axis. Here we are converting the
                // values into million pound.
                labelInterpolationFnc: function(value) {
                return  value ;
                }
            }
            };

            // All you need to do is pass your configuration as third parameter to the chart function
            new Chartist.Line('.ct-chart', data, options);


        }
        else{
            alert('You choose date out of bound');
            $("#start").val(new Date(label[0]));
            $("#end").val(new Date(label[label.length-1]));
        }
    });

 </script>
  <script type="text/javascript">
  

 </script>

</html>