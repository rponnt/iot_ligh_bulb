<!doctype html>
<html lang="Electric Controller">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Electric Controller</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M"
        crossorigin="anonymous">
    <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.1/css/bootstrap-slider.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">


    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <link href="{{URL::asset('css/slideBar.css')}}" rel="stylesheet">


    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.1/bootstrap-slider.min.js">
    </script>
    <script src="{{URL::asset('js/slideBar.js')}}"></script>

</head>

<body>
    <div class="container">
        <div class="row">
            <h1 class="display-1">Electric Controller</h1>
        </div>
         @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif

        
        <form action="{{url('/setting')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <div class="row">
                    <div class="col-6 col-md-4 col-xs-2">
                        <input type="hidden" name="light_name" value="{{$light_instance->name}}" />
                        <select id="exampleFormControlSelect2" name="instance" class="form-control" onchange="if (this.value) window.location.href=this.value">
                                    @foreach ($lights as $light)
                                            @if($light->name === $light_instance->name)
                                                <option value="{{ url('/item/'.$light->name) }}" selected>{{$light->name}}</option>
                                            
                                            @else
                                                <option value="{{ url('/item/'.$light->name) }}" >{{$light->name}}</option>

                                            @endif
                                        @endforeach

                                    </select>
                    </div>
                    <div class="col-6 col-md-4 col-xs-2">
                        <a href="{{ url('/remove/'.$light_instance->name) }}">
                            <button type="button" class="btn btn-danger form-control" name="btn-remove" onclick="confirm('Are you sure?'')">Remove</button>
                        </a>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label for="exampleFormControlSelect2" for="light">Light</label>
                        <input type="hidden" name="light" value="off" />
                         @if($light_instance->setting->light_status)
                            <input type="checkbox" checked data-toggle="toggle" data-onstyle="success" id="light" name="light" class="form-control" data-val="true">    
                        @else                  
                            <input type="checkbox" data-toggle="toggle" data-onstyle="success" id="light" name="light" class="form-control"data-val="true">   
                        @endif                    

                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label for="exampleFormControlSelect2" for="motion">Motion</label>
                        <input type="hidden" name="motion" value="off" /> 
                        @if($light_instance->setting->motion_status)
                            <input type="checkbox" checked data-toggle="toggle" data-onstyle="primary" id="motion" name="motion" class="form-control" data-val="true">     
                        @else                 
                            <input type="checkbox" data-toggle="toggle" data-onstyle="primary" id="motion" name="motion" class="form-control" data-val="true">
                        @endif                        

                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-md-4 col-xs-2">
                        <div class="range-slider">
                            <label for="exampleFormControlSelect2" for="dim">Dim(power of light)</label>
                            <input class="range-slider__range form-control" type="range" value="{{$light_instance->setting->dim}}"
                                min="0" max="100" name="dim" id="dim" oninput="dimOutput.value = dim.value">
                            <output name="dimOutput" id="dimOutput">{{$light_instance->setting->dim}}</output>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <label for="status">Motion Status :   </label> 
                    @if($light_instance->setting->motion_status )
                        <span class="badge badge-pill badge-warning"><p class="h5">Detect</p></span>
                     @else
                        <span class="badge badge-pill badge-secondary"><p class="h5">Not detect</p></span> 
                    @endif
                </div>
                <div class="row">
                    <label for="timeOut">Motion timeOut(second)</label>
                    <select id="timeOut" name="timeOut" class="form-control">
                                        <option val="{{$light_instance->setting->dim}}">
                                        {{$light_instance->setting->motion_time_out}}
                                        </option>
                                        <option val="6">6</option>
                                        <option val="10" >10</option>
                                        <option val="12">12</option>
                                        <option val="15">15</option>
                                        <option val="30">30</option>
                                        <option val="45">45</option>
                                        <option val="60">60</option>
                                </select>
                </div>
                <div class="row">
                    <input type="submit" class="btn btn-success btn-lg btn-block" name="btn-save" value="SAVE">
                       
                </div>
            </div>
        </form>


        <div class="form-group">
            <div class="row">
                <h3 class="display-3">Electric Add</h3>
            </div>

            <form method="post" action="{{url('/add')}}"accept-charset="UTF-8">
                {{csrf_field()}}
                    <div class="row">
                        <input type="text" class="form-control form-control-lg" placeholder="Instance Name" name="name">
                    </div>
                    <div class="row">
                        <textarea class="form-control form-control-lg" rows="5" placeholder="Instance Description" name="description"></textarea>
                    </div>
                    <div class="row">
                        <div class="col-6 col-md-4 col-xs-2">
                            <button type="submit" class="form-control "><p class="h3">Add</p></button>
                        </div>
                

                <div class="col-6 col-md-4 col-xs-2">
                    <button type="button" class="form-control form-control-lg btn btn-primary btn-lg btn-block"><p class="h3">Clear</p></button>
                </div>
        </form>
            </div>
        </div>

    </div>

</body>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js">
</script>
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js">
</script>
<script type="text/javascript" src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
</script>
<script type="text/javascript" src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js">
</script>
<script type="text/javascript">
    $('#datetimepicker').datetimepicker({



        format: 'dd-MM-yyyy hh:mm',
        language: 'en',
        collapse: false,
        maskInput: false,
        pickSeconds: false
    });
</script>
<script type="text/javascript">
    $('#datetimepicker2').datetimepicker({



        format: 'dd-MM-yyyy hh:mm',
        language: 'en',
        collapse: false,
        maskInput: false,
        pickSeconds: false
    });
</script>

</html>