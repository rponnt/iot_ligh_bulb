<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::resource('light', 'LightController');

Route::get('/', 'LightController@home');

Route::get('/item/{name}', 'LightController@view');

Route::post('/setting', 'LightController@setting');

Route::post('/add', 'LightController@add');

Route::get('/remove/{name}', 'LightController@remove');