<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('log', function (Blueprint $table) {
            //seting of light
            $table->uuid('id');
            $table->primary('id');
            $table->string('name',100);
            $table->boolean('light_status_on_artifact');
            $table->boolean('motion_status');
            $table->timestamp('time');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
