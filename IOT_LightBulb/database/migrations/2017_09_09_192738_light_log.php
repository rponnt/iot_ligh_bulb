<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LightLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('light_log', function (Blueprint $table) {
            //seting of light
            $table->uuid('id');
            $table->primary('id');
            $table->string('name',100)-> unique();
            $table->foreign('name')->references('name')->on('light_instance');
            $table->boolean('light_status_on_artifact');
            $table->boolean('motion_status');
            $table->dateTime('on_at');
            $table->dateTime('off_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('light_log');
    }
}
