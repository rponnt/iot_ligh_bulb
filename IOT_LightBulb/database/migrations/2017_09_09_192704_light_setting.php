<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LightSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('light_setting', function (Blueprint $table) {
        //seting of light
        $table->uuid('id');
        $table->primary('id');
        $table->string('name',100)-> unique();
        $table->foreign('name')->references('name')->on('light_instance');
        $table->boolean('light_status');
        $table->boolean('motion_status');
        $table->decimal('dim');
        $table->double('motion_time_out');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('light_setting');
    }
}
