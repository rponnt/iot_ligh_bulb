-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2017 at 06:52 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iot_lightbulb`
--

-- --------------------------------------------------------

--
-- Table structure for table `light_instance`
--

CREATE TABLE `light_instance` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `light_instance`
--

INSERT INTO `light_instance` (`id`, `name`, `description`, `remember_token`, `updated_at`, `created_at`) VALUES
('03621624-a12a-11e7-80f8-005056c00001', 'test', 'test', NULL, '2017-09-24 13:25:46', NULL),
('6.463382519137276e16', 'home light', 'homelight', NULL, '2017-11-07 10:48:32', '2017-11-07 10:48:32'),
('6.463382531474402e16', 'home miss u', 'home light', NULL, '2017-11-07 10:50:29', '2017-11-07 10:50:29'),
('b91e465a-a212-11e7-80f8-005056c00001', '11', 'testData', NULL, '2017-09-25 16:57:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `light_setting`
--

CREATE TABLE `light_setting` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `light_status` tinyint(1) NOT NULL,
  `motion_status` tinyint(1) NOT NULL,
  `dim` decimal(8,2) NOT NULL,
  `motion_time_out` double NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `light_setting`
--

INSERT INTO `light_setting` (`id`, `name`, `light_status`, `motion_status`, `dim`, `motion_time_out`, `updated_at`, `created_at`) VALUES
('2a34e49d-a12a-11e7-80f8-005056c00001', 'test', 1, 1, '41.00', 12, '2017-09-24 06:36:01', NULL),
('6.463382531474402e16', 'home miss u', 1, 1, '50.00', 12, '2017-11-07 10:50:30', '2017-11-07 10:50:30');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `light_status_on_artifact` tinyint(1) NOT NULL,
  `motion_status` tinyint(1) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `name`, `light_status_on_artifact`, `motion_status`, `time`) VALUES
('0c9dc886-a12b-11e7-80f8-005056c00001', 'test', 1, 0, '2017-08-31 17:00:00'),
('30e62cc3-a12b-11e7-80f8-005056c00001', 'test', 0, 1, '2017-09-25 17:00:00'),
('4cd5311a-a12a-11e7-80f8-005056c00001', 'test', 1, 0, '2017-09-24 13:13:47'),
('6c3c39cf-a12b-11e7-80f8-005056c00001', 'test', 0, 0, '2017-09-20 07:51:47'),
('73c8d41a-a12a-11e7-80f8-005056c00001', 'test', 1, 1, '2017-09-24 02:25:28'),
('984305f9-a202-11e7-80f8-005056c00001', '11', 0, 1, '2017-09-25 15:01:49'),
('a5b3ee21-a202-11e7-80f8-005056c00001', '11', 0, 0, '2017-09-02 11:00:00'),
('b1b14902-a202-11e7-80f8-005056c00001', '11', 0, 1, '2017-09-10 17:00:00'),
('c7e14923-a202-11e7-80f8-005056c00001', '11', 1, 1, '2017-09-24 13:51:34'),
('d7206e9c-a202-11e7-80f8-005056c00001', '11', 1, 0, '2017-09-14 17:00:00'),
('eb7a28e1-a12a-11e7-80f8-005056c00001', 'test', 1, 0, '2017-09-23 07:28:47');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2017_09_09_192608_light_instance', 1),
(6, '2017_09_09_192704_light_setting', 1),
(7, '2017_09_09_192738_light_log', 1),
(8, '2017_09_24_121042_create_log_table', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `light_instance`
--
ALTER TABLE `light_instance`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `light_instance_name_unique` (`name`);

--
-- Indexes for table `light_setting`
--
ALTER TABLE `light_setting`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `light_setting_name_unique` (`name`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `light_setting`
--
ALTER TABLE `light_setting`
  ADD CONSTRAINT `light_setting_name_foreign` FOREIGN KEY (`name`) REFERENCES `light_instance` (`name`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
